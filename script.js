function button() {
	let inp = document.getElementById("input").value;
	let data = {"input": inp}
	fetch('http://localhost:3000/topic', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json',
	},
	body: JSON.stringify(data),
	})
	.then(response => response.json())
	.then(data => {
		console.log('Success:', data);
	})
	.catch((error) => {
		console.error('Error:', error);
	});
	}
